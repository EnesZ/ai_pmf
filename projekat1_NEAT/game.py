from projekat1_NEAT.track import *
from projekat1_NEAT.car import *
import matplotlib.pyplot as plt
from projekat1_NEAT.model import Population
from random import randrange

#class Car()
track = Track(GRAY)


running =True
#game starts after we draw the track
game_started = False
drawing_inner_line = True
drawing_outer_line = False
drawing_car = False
sensors_on = False
race_again = False
all_sprites = pg.sprite.Group()
car = None
time2train=False
start_time = pg.time.get_ticks()
num_game = 0
state = None
action_list = []
num_frames = 0
reward_sum =0
population = None
while running:
    screen.fill(BLACK)
    for event in pg.event.get():
        if event.type == pg.KEYDOWN:
            if event.key == pg.K_ESCAPE:
                running = False
        if game_started and event.type ==pg.KEYDOWN:
            if event.key == pg.K_LEFT:
                car.steering('l')
            if event.key ==pg.K_RIGHT:
                car.steering('r')
            if event.key == pg.K_UP:
                car.accelerate()
            if event.key == pg.K_DOWN:
                car.brake()
            if event.key == pg.K_s:
                if sensors_on:
                    sensors_on = False
                else:
                    sensors_on=True
        if not game_started and event.type == pg.MOUSEBUTTONDOWN:
            #If player clicked on start button
            x, y = event.pos
            if WIDTH-110 <= x <=WIDTH and HEIGHT-60<=y<=HEIGHT:
                if drawing_inner_line:
                    drawing_inner_line=False
                    drawing_outer_line=True
                    break
                elif drawing_outer_line:
                    drawing_outer_line = False
                    drawing_car = True
                    break
                else:
                    game_started=True
                    start_time = pg.time.get_ticks()
                    for car in all_sprites:
                        car.calc_sensors2(track)
                        car.get_state(track)
                    print("**")
                    # get state
                    #state = reshape(state, [1, state_size])
                    break
            #If we are creating inner line
            if drawing_inner_line:
                track.set_inner((x,y))
            #If we are creating outer line
            if drawing_outer_line:
                track.set_outer((x,y))
            if drawing_car:
                population = Population(10,pg.math.Vector2(x,y))
                #car = Car(pg.math.Vector2(x,y))
                for car in population.population:
                    all_sprites.add(car)
                #car.draw_car(screen)
                all_sprites.draw(screen)
                drawing_car=False

    #Before game starts
    if drawing_inner_line:
        track.draw_inner(False)
    if drawing_outer_line:
        track.draw_outer()
        track.draw_inner(True)
    if not drawing_inner_line and not drawing_outer_line and not game_started:
        track.calc_reward_gates()
        track.draw_track()

    if game_started:
        track.draw_track()
        """
        car.calc_sensors2(track)

        if sensors_on:
            car.calc_sensors(track)
            car.draw_sensors(track)
        """
        i=0

        for car in all_sprites:

            action = car.model.act(car.state)
            if i<1:
                action = 1
            else:
                action=2
            i+=1
            #action = 3
            print(i)
            reward = car.update(track,action)
            if car.circle_intersection(track):
                all_sprites.remove(car)
            car.draw_car(screen)


                # every 30 sec or every time when agent hit border of the track we call replay to train
        if start_time:
            time_since_enter = (pg.time.get_ticks() - start_time) / 1000
            if time_since_enter > 20:
                # agent.replay(32)
                start_time = pg.time.get_ticks()
                # time2train = True
                race_again = True

        if len(all_sprites)==0 or race_again:

            population.calc_fitness()

            sel_time = pg.time.get_ticks()
            population.selection()

            all_sprites.empty()
            for car in population.population:
                car.calc_sensors2(track)
                car.get_state(track)
                all_sprites.add(car)
            race_again = False
            start_time = pg.time.get_ticks()

            num_game+=1


    if not game_started:
        # draw button and text
        pg.draw.rect(screen,RED,(WIDTH-110,HEIGHT-60,100,50))
        msg ="Draw next"
        if drawing_car:
            msg = "Start"
        text = FONT.render(msg, True, WHITE)
        text_rect = text.get_rect(center=(WIDTH-60, HEIGHT-35))
        screen.blit(text, text_rect)
    #all_sprites.draw(screen)

    screen.blit(FONT.render("Game: {}".format(num_game), True, RED), (2, 40))
    pg.display.flip()

#print(agent.model_history)

plt.show()
pg.quit()

