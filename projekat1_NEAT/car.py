from projekat1_NEAT.config import *
from projekat1_NEAT.track import *
import math
import projekat1_NEAT.model
from random import uniform



max_speed = 5
class Car(pg.sprite.Sprite):
    def __init__(self,position):
        pg.sprite.Sprite.__init__(self)
        self.image = pg.Surface((10,10))

        self.image.fill(WHITE)
        self.image.set_colorkey(WHITE)
        self.rect = self.image.get_rect()
        self.position = position

        self.rect.center = position
        pg.draw.circle(self.image, BLACK, (int(position.x), int(position.y)), 10)
        self.velocity = pg.math.Vector2(0,-10)
        self.velocity_magnitude = 0
        self.speed = 0.05
        self.velocity.scale_to_length(self.speed)
        self.start_position = pg.math.Vector2(position.x,position.y)
        self.sensors = None
        self.sensors2 = None
        self.theta=None

        self.model = projekat1_NEAT.model.Model()
        self.fitness_sum = 0
        self.state = None
        self.track_active_gate = 2
        self.track_width = None
        self.track_axis = None

    def crossover(self,car):
        sel_time = pg.time.get_ticks()
        weights1 = self.model.nn.get_weights()
        weights2 = car.model.nn.get_weights()

        new_weights =  weights1
        new_weights[0] = weights2[0]
        # mutate

        sel_time = pg.time.get_ticks()
        #car = Car(self.start_position)


        for matrix_i in range(len(new_weights)):
            for row_i in range(len(new_weights[matrix_i])):
                for i in range(len(new_weights[matrix_i][row_i])):

                    new_weights[matrix_i][row_i][i] = round(new_weights[matrix_i][row_i][i],3)
                    #new_weights[matrix_i][row_i][i] = 0

                    if uniform(0,1)>1-mutation_rate:
                        change = uniform(-0.5,0.5)
                        new_weights[matrix_i][row_i][i]+=change

        return new_weights
        """
        self.model.nn.set_weights(new_weights)
        self.track_active_gate = 2
        self.track_axis = None
        self.track_width = None
        
        self.position = pg.math.Vector2(self.start_position.x,self.start_position.y)
        self.fitness_sum = 0
        """

        #car.model.nn.set_weights(new_weights)

        #return car

    #def mutate(self):


    #set direction of the car in the beginning
    def draw_car(self,screen):
        pg.draw.circle(screen, BLUE, (int(self.position.x), int(self.position.y)), CAR_RADIUS)

    def set_velocity(self,point):
        self.velocity = point-self.position
        self.speed=1
        self.velocity.scale_to_lenght(self.speed)
    def steering(self,key):
        if key=="r":
            self.velocity.rotate_ip(10)
            #self.image=rot_center(self.image,10)
        else:
            self.velocity.rotate_ip(-10)
            #self.image=rot_center(self.image,-10)
    def accelerate(self):
        if self.speed <max_speed:
            self.speed+=0.005
    def brake(self):
        if self.speed>0.015:
            self.speed-=0.005
    def update(self,track,action):
        #pg.draw.line(screen,RED,self.position,self.position+self.velocity)

        if action==0:
            self.steering("l")
        elif action==1:
            self.steering("r")


        self.draw_direction()
        #self.draw_sensors(track)
        self.velocity.scale_to_length(self.speed)
        self.position+=self.velocity
        self.rect.center = self.position
        if self.track_width == None:
            track_axis, track_width = track.calc_track_axis(self.track_active_gate)
            self.track_axis = track_axis
            self.track_width = track_width
        angle = self.calc_angle(track)
        self.theta=angle
        #reward = self.gate_intersection(track)
        reward = self.gate_circle_intersection(track,angle)
        self.fitness_sum+=reward
        return reward
    """
    def intersection(self,track):
        p1 = self.rect.topleft
        p2 = self.rect.topright
        p3 = self.rect.bottomright
        p4 = self.rect.bottomleft

        rect_vertices =(p1,p2,p3,p4)

        if poly_intersection(track.inner_line,rect_vertices):
            return True
        if poly_intersection(track.outer_line,rect_vertices):
            return True
        return False
    """
    def circle_intersection(self,track):
        for i in range(len(track.outer_line)):
            if p2s_distance(self.rect.center,(track.outer_line[i-1],track.outer_line[i]))<CAR_RADIUS:
                return True
        for i in range(len(track.inner_line)):
            if p2s_distance(self.rect.center,(track.inner_line[i-1],track.inner_line[i]))<CAR_RADIUS:

                return True
        return False




    def get_direction(self):
        velocity_temp = pg.math.Vector2(self.velocity.x, self.velocity.y)
        velocity_temp.scale_to_length(50)
        v_temp = self.position + velocity_temp
        return v_temp
    def draw_direction(self):
        v_temp = self.get_direction()
        v_temp2 = pg.math.Vector2(self.velocity.x, self.velocity.y)
        #v_temp2.scale_to_length(WIDTH)
        #s1 = self.position + v_temp2.rotate(10)
        #s2 = self.position + v_temp2.rotate(-10)
        #pg.draw.line(screen,RED,(0,0),velocity_temp)
        pg.draw.line(screen,RED,self.position,v_temp,2)
        #pg.draw.line(screen,RED,self.position,s1,2)

        #pg.draw.line(screen,RED,self.position, s2,2)
        #pg.draw.circle(screen,RED,(int(velocity_temp.x),int(velocity_temp.y)),3)
    def calc_sensor(self,track,p1,p2):
        # returns diagonal or vertical sensor from line p1,p2
        intersected_segments = []
        for i in range(len(track.outer_line)):
            if segment_intersection((p1,p2),(track.outer_line[i-1],track.outer_line[i])):
                intersected_segments.append((track.outer_line[i-1],track.outer_line[i]))
        for i in range(len(track.inner_line)):
            if segment_intersection((p1, p2), (track.inner_line[i - 1], track.inner_line[i])):
                intersected_segments.append((track.inner_line[i - 1], track.inner_line[i]))

        intersected_points = []
        for s in intersected_segments:
            a_temp,b_temp = p2p_line(s[0],s[1])
            a, b = p2p_line(self.rect.center, p2)
            x,y = line_intersection(a,b,a_temp,b_temp)
            #if intersected point is between p1 and p2
            if abs(distance(p1,(x,y))+distance((x,y),p2)-distance(p1,p2))<EPSILON:
                intersected_points.append((x,y))

        sensor_point = min(intersected_points,key=lambda p: distance(p1,p))
        return p1,sensor_point

    def calc_sensors(self,track):
        #calc diagonal and horizontal sensors as segment
        sensors = []
        start_points=(self.rect.topleft,self.rect.topright,self.rect.bottomright,self.rect.bottomleft,\
                      self.rect.midleft,self.rect.midright)
        ref_points = (0,WIDTH,WIDTH,0,0,WIDTH)
        for i in range(len(start_points)):
            # sensor is line through rect center and start point[i]
            a, b = p2p_line(self.rect.center, start_points[i])
            p1 = start_points[i]
            p2 = (ref_points[i], a * ref_points[i] + b)
            #p1,p2 = self.calc_sensor(track,start_points[i],ref_points[i])
            p1, p2 = self.calc_sensor(track, p1, p2)
            sensors.append((p1,p2))
            #pg.draw.line(screen,GREEN,p1,p2,1)
            #pg.draw.circle(screen,GREEN,(int(p2[0]),int(p2[1])),2)

        #calc vertical sensors
        intersected_segments = []
        temp_p = self.rect.center
        for i in range(len(track.inner_line)):
            #if both x-coors of segments end points are not greater/lesser than rect center
            if track.inner_line[i-1][0]>temp_p[0] and track.inner_line[i][0]>temp_p[0]:
                continue
            if track.inner_line[i-1][0]<temp_p[0] and track.inner_line[i][0]<temp_p[0]:
                continue
            intersected_segments.append((track.inner_line[i-1],track.inner_line[i]))
        for i in range(len(track.outer_line)):
            if track.outer_line[i-1][0]>temp_p[0] and track.outer_line[i][0]>temp_p[0]:
                continue
            if track.outer_line[i-1][0]<temp_p[0] and track.outer_line[i][0]<temp_p[0]:
                continue
            intersected_segments.append((track.outer_line[i-1],track.outer_line[i]))
        intersected_points = []
        for s in intersected_segments:
            a_temp,b_temp = p2p_line(s[0],s[1])
            x_temp = temp_p[0]
            y_temp = a_temp*x_temp+b_temp
            intersected_points.append((x_temp,y_temp))
        top_int_points=[]
        bottom_int_points = []
        for p in intersected_points:
            if p[1]-temp_p[1]>0:
                bottom_int_points.append(p)
            else:
                top_int_points.append(p)

        top_p = min(top_int_points,key=lambda p: temp_p[1]-p[1])
        bottom_p = min(bottom_int_points,key=lambda p: p[1]-temp_p[1])
        sensors.append((self.rect.midtop,top_p))
        sensors.append((self.rect.midbottom,bottom_p))
        #pg.draw.line(screen, GREEN, temp_p,top_p , 1)
        #pg.draw.line(screen, GREEN, temp_p, bottom_p,1)
        #pg.draw.circle(screen, GREEN, (int(top_p[0]), int(top_p[1])), 2)
        #pg.draw.circle(screen, GREEN, (int(bottom_p[0]), int(bottom_p[1])), 2)
        self.sensors = sensors
    def draw_sensors(self,track,type=1):
        if type==1:
            self.calc_sensors(track)
            for s in self.sensors:
                pg.draw.line(screen,GREEN,s[0],s[1],1)
                pg.draw.circle(screen,GREEN,(int(s[1][0]),int(s[1][1])),2)
        else:
            self.calc_sensors2(track)
            for s in self.sensors2:
                pg.draw.line(screen, ORANGE, s[0], s[1], 3)
                #pg.draw.circle(screen, ORANGE, (int(s[1][0]), int(s[1][1])), 3)

    def calc_sensors2(self,track):
        v_temp = pg.math.Vector2(self.velocity.x, self.velocity.y)
        v_temp.scale_to_length(WIDTH)
        s4 = self.position + v_temp.rotate(10)
        s3 = self.position + v_temp.rotate(-10)
        s6 = self.position + v_temp.rotate(90)
        s1 = self.position + v_temp.rotate(-90)

        s2 = self.position + v_temp.rotate(-45)
        s5 = self.position + v_temp.rotate(45)
        s7 = self.position + v_temp
        full_sensors = [s1,s2,s3,s4,s5,s6,s7]
        sensors = []

        for s in full_sensors:
            p1,p2 = self.calc_sensor(track,self.rect.center,(s.x,s.y))
            sensors.append((p1,p2))
        self.sensors2 = sensors

    """
    def gate_intersection(self,track):
        p1 = self.rect.topleft
        p2 = self.rect.topright
        p3 = self.rect.bottomright
        p4 = self.rect.bottomleft
        rect_vertices = (p1, p2, p3, p4)

        if poly_intersection(rect_vertices,track.reward_gates[track.active_gate]):

            reward = 5
            # if it is the last gate
            if track.active_gate==1:
                reward = 25
            x,y,z = track.next_gate(self.track_active_gate)
            self.track_active_gate = x
            self.track_axis = y
            self.track_width = z
            #return reward
            return reward
        return -1
    """


    def gate_circle_intersection(self,track,angle):

        if p2s_distance(self.rect.center, track.reward_gates[self.track_active_gate])<CAR_RADIUS:
            reward = 5
            # if it is the last gate
            if self.track_active_gate == 1:
                reward = 25
            x, y, z = track.next_gate(self.track_active_gate)
            self.track_active_gate = x
            self.track_axis = y
            self.track_width = z
            # return reward
            return reward
        if angle:
            #normalized distance between track and track axis


            dist2axis = p2s_distance(self.rect.center,self.track_axis)
            dist_norm = dist2axis/self.track_width
            theta_rad = math.radians(angle)
            reward = 0.03 * self.speed*(math.cos(theta_rad)-math.sin(theta_rad)-dist_norm)
            return reward
        return 0


    def get_state(self,track):
        # returns state of the agent
        state = []

        d = p2s_distance(self.rect.center,track.reward_gates[self.track_active_gate])
        state.append(d)
        state.append(self.theta)
        #for n in self.rect.center:
        #    state.append(n)
        """
        for n in self.velocity.normalize():
            state.append(n)
        i=0
        """
        for s in self.sensors2:
            #if i==4:
            #    break
            d = distance(s[0],s[1])-CAR_RADIUS
            state.append(d)
            #i+=1
        state = reshape(state, [1, state_size])
        self.state = state
        #return state

    def calc_angle(self,track):
        #calculates angle between track axis and velocity
        v_temp = self.get_direction()
        v_temp = v_temp-self.position

        if self.track_axis:
            axis_vec = pg.Vector2(self.track_axis[1][0],self.track_axis[1][1])\
                       -pg.Vector2(self.track_axis[0][0],self.track_axis[0][1])
            theta = v_temp.angle_to(axis_vec)
            return theta






