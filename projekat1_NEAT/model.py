import os
os.environ['KERAS_BACKEND'] = 'theano'
from keras.models import Sequential
from keras.layers import Dense,BatchNormalization, Dropout
from keras.optimizers import Adam
from projekat1_NEAT.config import state_size
import numpy as np
import projekat1_NEAT.car
import random
import bisect
import pygame as pg
import gc




class Model:
    def __init__(self):
        self.nn = Sequential()
        self.nn.add(Dense(16, input_dim=state_size, activation='relu',use_bias=False))
        self.nn.add(Dense(8, activation='relu',use_bias=False))
        self.nn.add(Dense(3, activation='relu',use_bias=False))
        self.fitness = 0
    def act(self,state):
        act_values = self.nn.predict(state)
        return np.argmax(act_values[0])

class Population:
    def __init__(self,population_size,start_position):
        self.size = population_size
        self.population=[]
        for i in range(self.size):
            self.population.append(projekat1_NEAT.car.Car(start_position))
        self.generation = 0
    def calc_fitness(self):
        sum = 0
        fitness_list = []
        for car in self.population:
            fitness_list.append(car.fitness_sum)
        min_elem = min(fitness_list)
        print(fitness_list)
        if min_elem<0:
            temp = (-1)*min_elem+0.001
            for i in range(len(fitness_list)):
                fitness_list[i]+=temp
        print(fitness_list)

        """
        print("first population len {} ".format(len(self.population)))
        print("20 {}".format(self.population[20].fitness_sum))
        min_elem = min(self.population, key=lambda x : x.fitness_sum)
        print("minim {}".format(min_elem.fitness_sum))
        if min_elem.fitness_sum <0:

            abs_fittness_sum = (-1)*min_elem.fitness_sum
            i=0
            for car in self.population:

                if i==20:
                    print("bef {} {}".format(i,car.fitness_sum))
                #car.fitness_sum += abs_fittness_sum
                if i==20:
                    print("after {} {}".format(i, car.fitness_sum))
                i+=1
                print("i - {} value {}".format(i, car.fitness_sum))
        i=0
        for car in self.population:
            print("i - {} value {}".format(i, car.fitness_sum))
            sum+=car.fitness_sum
            i+=1
        """
        for n in fitness_list:
            sum+=n
        """
        print("2nd population len {} ".format(len(self.population)))
        print("20 {}".format(self.population[20].fitness_sum))
        for car in self.population:
            car.fitness_sum/=sum
        """
        for i in range(len(fitness_list)):
            fitness_list[i]/=sum
        i = 0
        for car in self.population:
            car.fitness_sum = fitness_list[i]
            i+=1


    def selection(self):
        new_population = []
        reproduction = []
        result = []
        sum = 0

        for e in self.population:
            sum+= e.fitness_sum
            result.append(sum)



        for i in range(self.size):
            x = random.uniform(0,0.999)
            idx = bisect.bisect_left(result,x)
            reproduction.append(self.population[idx])

        sel_time = pg.time.get_ticks()
        weights_list = []
        for i in range(self.size-1):
            #elem = reproduction[i].crossover(reproduction[i+1])
            weights = reproduction[i].crossover(reproduction[i + 1])
            #new_population.append(reproduction[i])
            weights_list.append(weights)
        #elem = reproduction[0].crossover(reproduction[self.size-1])
        weights = reproduction[self.size-1].crossover(reproduction[0])
        #new_population.append(reproduction[self.size-1])
        weights_list.append(weights)
        for i in range(self.size-1):
            c = projekat1_NEAT.car.Car(reproduction[i].start_position)
            """
            reproduction[i].model.nn.set_weights(weights_list[i])
            reproduction[i].track_active_gate = 2
            reproduction[i].track_axis = None
            reproduction[i].track_width = None
            reproduction[i].position = pg.math.Vector2(reproduction[i].start_position.x,reproduction[i].start_position.y)
            reproduction[i].fitness_sum = 0
            """
            c.model.nn.set_weights(weights_list[i])
            new_population.append(c)

        print((pg.time.get_ticks() - sel_time) / 1000)

        self.population = new_population
        self.generation += 1
        print("Done")
