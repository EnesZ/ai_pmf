from projekat1AI.track import *
from projekat1AI.car import *
import matplotlib.pyplot as plt

#class Car()
track = Track(YELLOW)


running =True
#game starts after we draw the track
game_started = False
drawing_inner_line = True
drawing_outer_line = False
drawing_car = False
sensors_on = False
race_again = False
all_sprites = pg.sprite.Group()
car = None
time2train=False
start_time = pg.time.get_ticks()
num_game = 0
state = None
while running:
    screen.fill(BLACK)
    for event in pg.event.get():
        if event.type == pg.KEYDOWN:
            if event.key == pg.K_ESCAPE:
                running = False
        if game_started and event.type ==pg.KEYDOWN:
            if event.key == pg.K_LEFT:
                car.steering('l')
            if event.key ==pg.K_RIGHT:
                car.steering('r')
            if event.key == pg.K_UP:
                car.accelerate()
            if event.key == pg.K_DOWN:
                car.brake()
            if event.key == pg.K_s:
                if sensors_on:
                    sensors_on = False
                else:
                    sensors_on=True
        if not game_started and event.type == pg.MOUSEBUTTONDOWN:
            #If player clicked on start button
            x, y = event.pos
            if WIDTH-110 <= x <=WIDTH and HEIGHT-60<=y<=HEIGHT:
                if drawing_inner_line:
                    drawing_inner_line=False
                    drawing_outer_line=True
                    break
                elif drawing_outer_line:
                    drawing_outer_line = False
                    drawing_car = True
                    break
                else:
                    game_started=True
                    start_time = pg.time.get_ticks()
                    car.calc_sensors(track)
                    # get state
                    state = car.get_state(track)
                    state = reshape(state, [1, state_size])
                    break
            #If we are creating inner line
            if drawing_inner_line:
                track.set_inner((x,y))
            #If we are creating outer line
            if drawing_outer_line:
                track.set_outer((x,y))
            if drawing_car:
                car = Car(pg.math.Vector2(x,y))
                all_sprites.add(car)
                drawing_car=False

    #Before game starts
    if drawing_inner_line:
        track.draw_inner(False)
    if drawing_outer_line:
        track.draw_outer()
        track.draw_inner(True)
    if not drawing_inner_line and not drawing_outer_line and not game_started:
        track.calc_reward_gates()
        track.draw_track()

    if game_started:
        track.draw_track()
        car.calc_sensors(track)

        if sensors_on:
            car.draw_sensors(track)

        first_distance2gate = state[0][0]

        action = agent.act(state)

        reward = car.update(track,action)
        if reward>1:
            start_time = pg.time.get_ticks()
        #print(reward)
        next_state = car.get_state(track)
        next_state = reshape(next_state, [1, state_size])
        second_distance2gate = next_state[0][0]

        if reward==-1:
            if first_distance2gate>second_distance2gate:
                reward=0.1
            else:
                reward = -0.1

        # every 30 sec or every time when agent hit border of the track we call replay to train
        if start_time:
            time_since_enter = (pg.time.get_ticks() - start_time) / 1000
            if time_since_enter > 30:
                time2train = True
        if car.intersection(track) or time2train:
            #if car.intersection(track):
            reward = -5
            start_position = car.start_position
            all_sprites.empty()
            del car
            car = Car(start_position)
            all_sprites.add(car)
            track.active_gate=track.starting_gate
            car.calc_sensors(track)
            race_again = True
            #running=False
        # Remember the previous state, action, reward,
        agent.remember(state,action,reward,next_state,race_again)
        state = next_state
        if race_again:
            agent.replay(32)
            race_again=False
            time2train=False
            start_time = pg.time.get_ticks()
            num_game+=1
            # get state
            state = car.get_state(track)
            state = reshape(state, [1, state_size])


    if not game_started:
        # draw button and text
        pg.draw.rect(screen,RED,(WIDTH-110,HEIGHT-60,100,50))
        msg ="Next"

        text = FONT.render(msg, True, WHITE)
        text_rect = text.get_rect(center=(WIDTH-60, HEIGHT-35))
        screen.blit(text, text_rect)
    all_sprites.draw(screen)
    screen.blit(FONT.render("Game: {}".format(num_game), True, RED), (2, 40))
    pg.display.flip()

#print(agent.model_history)
plt.plot(agent.model_history)
#plt.show()
pg.quit()

