import pygame as pg
from projekat1AI.constants import screen,BLACK,segment_center,ORANGE

class Track():
    def __init__(self,color):
        self.inner_line=[]
        self.outer_line=[]
        self.color=color
        self.reward_gates = None
        self.active_gate = None
        self.starting_gate = 2
    def set_inner(self,point):
        self.inner_line.append(point)
    def set_outer(self,point):
        self.outer_line.append(point)
    def draw_inner(self,closed):
        #while we are creating inner line of the track we will draw point/line
        if len(self.inner_line)==1:
            pg.draw.circle(screen,self.color,self.inner_line[0],3)
        elif len(self.inner_line)>1:
            pg.draw.lines(screen,self.color,closed,self.inner_line)
            #pg.draw.polygon(screen, self.color, self.inner_line)
    def draw_outer(self):
        # while we are creating outer line of the track we will draw point/line
        if len(self.outer_line) == 1:
            pg.draw.circle(screen, self.color, self.outer_line[0], 3)
        elif len(self.outer_line) > 1:
            pg.draw.lines(screen, self.color, False, self.outer_line)
    def draw_track(self):
        pg.draw.polygon(screen,self.color,self.outer_line)
        pg.draw.polygon(screen,BLACK,self.inner_line)
        self.draw_reward_gates()
    def calc_reward_gates(self):
        if self.reward_gates:
            return None
        gate_list = []
        for i in range(len(self.inner_line)):
            gate_list.append((self.inner_line[i-1],self.outer_line[i-1]))
            x1,y1 = segment_center(self.inner_line[i-1],self.inner_line[i])
            x2,y2 = segment_center(self.outer_line[i-1],self.outer_line[i])
            gate_list.append(((x1,y1),(x2,y2)))
        self.reward_gates=gate_list
        self.active_gate=2
    def draw_reward_gates(self):
        if self.reward_gates:
            for i in range(len(self.reward_gates)):
                width=2
                if self.active_gate==i:
                    width=6
                pg.draw.line(screen,ORANGE,self.reward_gates[i][0],self.reward_gates[i][1],width)

            #for gate in self.reward_gates:
            #    pg.draw.line(screen,ORANGE,gate[0],gate[1],2)
    def next_gate(self):
        if self.active_gate<len(self.reward_gates)-1:
            self.active_gate+=1
        else:
            self.active_gate=0

