from projekat1AI.constants import *
from projekat1AI.track import *

max_speed = 5
class Car(pg.sprite.Sprite):
    def __init__(self,position):
        pg.sprite.Sprite.__init__(self)
        self.image = pg.Surface((20,20))
        self.image.fill(RED)
        self.rect = self.image.get_rect()
        self.position = position
        self.rect.center = position
        self.velocity = pg.math.Vector2(0,-10)
        self.velocity_magnitude = 0
        self.speed = 0.05
        self.velocity.scale_to_length(self.speed)
        self.start_position = pg.math.Vector2(position.x,position.y)
        self.sensors = None
    #set direction of the car in the beginning
    def set_velocity(self,point):
        self.velocity = point-self.position
        self.speed=1
        self.velocity.scale_to_lenght(self.speed)
    def steering(self,key):
        if key=="r":
            self.velocity.rotate_ip(10)
            #self.image=rot_center(self.image,10)
        else:
            self.velocity.rotate_ip(-10)
            #self.image=rot_center(self.image,-10)
    def accelerate(self):
        if self.speed <max_speed:
            self.speed+=0.005
    def brake(self):
        if self.speed>0.005:
            self.speed-=0.005
    def update(self,track,action):
        #pg.draw.line(screen,RED,self.position,self.position+self.velocity)
        if action==0:
            self.steering("l")
        elif action==1:
            self.steering("r")
        self.draw_direction()
        #self.draw_sensors(track)
        self.velocity.scale_to_length(self.speed)
        self.position+=self.velocity
        self.rect.center = self.position
        reward = self.gate_intersection(track)
        return reward

    def intersection(self,track):
        p1 = self.rect.topleft
        p2 = self.rect.topright
        p3 = self.rect.bottomright
        p4 = self.rect.bottomleft

        rect_vertices =(p1,p2,p3,p4)

        if poly_intersection(track.inner_line,rect_vertices):
            return True
        if poly_intersection(track.outer_line,rect_vertices):
            return True
        return False
    def draw_direction(self):
        velocity_temp = pg.math.Vector2(self.velocity.x,self.velocity.y)
        velocity_temp.scale_to_length(50)
        v_temp = self.position+velocity_temp
        #pg.draw.line(screen,RED,(0,0),velocity_temp)
        pg.draw.line(screen,RED,self.position,v_temp,2)
        #pg.draw.circle(screen,RED,(int(velocity_temp.x),int(velocity_temp.y)),3)
    def calc_sensor(self,track,start_p,ref_p):
        # return diagonal or vertical sensor in regards to start and ref point
        a,b = p2p_line(self.rect.center,start_p)
        p1 = start_p
        p2 = (ref_p,a*ref_p+b)
        intersected_segments = []
        for i in range(len(track.outer_line)):
            if segment_intersection((p1,p2),(track.outer_line[i-1],track.outer_line[i])):
                intersected_segments.append((track.outer_line[i-1],track.outer_line[i]))
        for i in range(len(track.inner_line)):
            if segment_intersection((p1, p2), (track.inner_line[i - 1], track.inner_line[i])):
                intersected_segments.append((track.inner_line[i - 1], track.inner_line[i]))

        intersected_points = []
        for s in intersected_segments:
            a_temp,b_temp = p2p_line(s[0],s[1])
            x,y = line_intersection(a,b,a_temp,b_temp)
            #if intersected point is between p1 and p2
            if abs(distance(p1,(x,y))+distance((x,y),p2)-distance(p1,p2))<EPSILON:
                intersected_points.append((x,y))

        sensor_point = min(intersected_points,key=lambda p: distance(p1,p))
        return p1,sensor_point

    def calc_sensors(self,track):
        #calc diagonal and horizontal sensors as segment
        sensors = []
        start_points=(self.rect.topleft,self.rect.topright,self.rect.bottomright,self.rect.bottomleft,\
                      self.rect.midleft,self.rect.midright)
        ref_points = (0,WIDTH,WIDTH,0,0,WIDTH)
        for i in range(len(start_points)):
            p1,p2 = self.calc_sensor(track,start_points[i],ref_points[i])
            sensors.append((p1,p2))
            #pg.draw.line(screen,GREEN,p1,p2,1)
            #pg.draw.circle(screen,GREEN,(int(p2[0]),int(p2[1])),2)

        #calc vertical sensors
        intersected_segments = []
        temp_p = self.rect.center
        for i in range(len(track.inner_line)):
            #if both x-coors of segments end points are not greater/lesser than rect center
            if track.inner_line[i-1][0]>temp_p[0] and track.inner_line[i][0]>temp_p[0]:
                continue
            if track.inner_line[i-1][0]<temp_p[0] and track.inner_line[i][0]<temp_p[0]:
                continue
            intersected_segments.append((track.inner_line[i-1],track.inner_line[i]))
        for i in range(len(track.outer_line)):
            if track.outer_line[i-1][0]>temp_p[0] and track.outer_line[i][0]>temp_p[0]:
                continue
            if track.outer_line[i-1][0]<temp_p[0] and track.outer_line[i][0]<temp_p[0]:
                continue
            intersected_segments.append((track.outer_line[i-1],track.outer_line[i]))
        intersected_points = []
        for s in intersected_segments:
            a_temp,b_temp = p2p_line(s[0],s[1])
            x_temp = temp_p[0]
            y_temp = a_temp*x_temp+b_temp
            intersected_points.append((x_temp,y_temp))
        top_int_points=[]
        bottom_int_points = []
        for p in intersected_points:
            if p[1]-temp_p[1]>0:
                bottom_int_points.append(p)
            else:
                top_int_points.append(p)

        top_p = min(top_int_points,key=lambda p: temp_p[1]-p[1])
        bottom_p = min(bottom_int_points,key=lambda p: p[1]-temp_p[1])
        sensors.append((self.rect.midtop,top_p))
        sensors.append((self.rect.midbottom,bottom_p))
        #pg.draw.line(screen, GREEN, temp_p,top_p , 1)
        #pg.draw.line(screen, GREEN, temp_p, bottom_p,1)
        #pg.draw.circle(screen, GREEN, (int(top_p[0]), int(top_p[1])), 2)
        #pg.draw.circle(screen, GREEN, (int(bottom_p[0]), int(bottom_p[1])), 2)
        self.sensors = sensors
    def draw_sensors(self,track):
        self.calc_sensors(track)
        for s in self.sensors:
            #print(s[0])
            pg.draw.line(screen,GREEN,s[0],s[1],1)
            pg.draw.circle(screen,GREEN,(int(s[1][0]),int(s[1][1])),2)

    def gate_intersection(self,track):
        p1 = self.rect.topleft
        p2 = self.rect.topright
        p3 = self.rect.bottomright
        p4 = self.rect.bottomleft
        rect_vertices = (p1, p2, p3, p4)

        if poly_intersection(rect_vertices,track.reward_gates[track.active_gate]):

            reward = 20
            # if it is the last gate
            if track.active_gate==1:
                reward = 250
            track.next_gate()
            #return reward
            return reward
        return -1
    def get_state(self,track):
        # returns state of the agent
        state = []

        d = p2s_distance(self.rect.center,track.reward_gates[track.active_gate])
        state.append(d)
        #for n in self.rect.center:
        #    state.append(n)

        for n in self.velocity.normalize():
            state.append(n)
        i=0
        for s in self.sensors:
            #if i==4:
            #    break
            d = distance(s[0],s[1])
            state.append(d)
            i+=1
        return state






