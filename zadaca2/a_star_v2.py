import pandas as pd

#read data
df_data = pd.read_csv('res/a_star_data.csv',delimiter=';')
df_2ulm = pd.read_csv('res/distance2ulm.csv',delimiter=';')

cities = df_data.columns.tolist()[1:]
cities_dict = {city : [] for city in cities}

#print(cities_dict)
#print(cities)

city_matrix = df_data.values
#create city graph as dictionary
for row in city_matrix:
    city = row[0]
    for i in range(len(row[1:])):
        if row[i+1]>0:
            cities_dict[city].append((cities[i],row[i+1]))

# heuristic function, air distance beetwen Ulm and cities
distance2ulm_dict = {row[0]:row[1] for row in df_2ulm.values}
distance2ulm_dict['Ulm']=0
#print(distance2ulm_dict)

class Node():
    def __init__(self,h,g,city,p):
        #distance from start
        self.g = g
        self.f = h+g
        self.city = city
        self.parent = p


child2parent = {}

def a_star(start_node, goal,node_list):

    node_list.append(start_node)
    child2parent[start_node.city]=start_node.parent

    while True:
        if not node_list:
            return "No solution"

        node = node_list[0]
        child2parent[node.city]=node.parent
        node_list = node_list[1:]

        if node.city == goal:
            print(node.g)
            path = []
            current_city = node.city
            while current_city:
                path.append(current_city)
                current_city = child2parent[current_city]
            return path[::-1]
        succesors = cities_dict[node.city]

        for s in succesors:
            if s[0] == node.parent:
                continue
            node_list.append(Node(distance2ulm_dict[s[0]],s[1]+node.g,s[0],node.city))
        node_list = sorted(node_list,key= lambda x : x.f)

start_city = 'Frankfurt'
# we only have h(x) for Ulm so it works only for Ulm
goal_city = 'Ulm'
node_list = []
start_node = Node(distance2ulm_dict[start_city],0,start_city,None)

path2goal = a_star(start_node,goal_city,node_list)

print(path2goal)

#print(cities_dict)
#print(df_data.values)



