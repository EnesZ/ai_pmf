import pandas as pd

df_data = pd.read_csv('res/a_star_data.csv',delimiter=';')
df_2ulm = pd.read_csv('res/distance2ulm.csv',delimiter=';')

cities = df_data.columns.tolist()[1:]
cities_dict = {city : [] for city in cities}

#print(cities_dict)
#for city in cities:
#    cities_dict[city]=[]

#print(cities)
city_matrix = df_data.values
for row in city_matrix:
    city = row[0]
    for i in range(len(row[1:])):
        if row[i+1]>0:
            cities_dict[city].append((cities[i],row[i+1]))

distance2ulm_dict = {row[0]:row[1] for row in df_2ulm.values}
distance2ulm_dict['Ulm']=0
print(distance2ulm_dict)

class Node():
    def __init__(self,h,g,city,p):
        #distance from start
        self.g = g
        self.f = h+g
        self.city = city
        self.path = p
        self.path.append(city)

node_list = []
def a_star(start_node, goal,node_list):

    node_list.append(start_node)

    while True:
        if not node_list:
            return "No solution"
        node = node_list[0]
        node_list = node_list[1:]
        if node.city == goal:
            print(node.g)
            return node.path
        succesors = cities_dict[node.city]

        for s in succesors:
            path = node.path.copy()
            if s[0] in node.path:
                continue
            node_list.append(Node(distance2ulm_dict[s[0]],s[1]+node.g,s[0],path))
        node_list = sorted(node_list,key= lambda x : x.f)

start_city = 'Frankfurt'
# we only have h(x) for Ulm so it works only for Ulm
goal_city = 'Ulm'

start_node = Node(distance2ulm_dict[start_city],0,start_city,[])

path2goal = a_star(start_node,goal_city,node_list)

print(path2goal)

#print(cities_dict)
#print(df_data.values)



