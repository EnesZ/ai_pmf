merge([],L,L).
merge(L1,L2,R):-
    L1 = [Head1|Tail1],
    R = [Head2|Tail2],
    Head1 = Head2,
    merge(Tail1,L2,Tail2).
