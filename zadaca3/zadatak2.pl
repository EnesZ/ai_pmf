male(james1).
male(charles1).
male(charles2).
male(james2).
male(george1).

female(catherine).
female(elizabeth).
female(sophia).

parent(james1, charles1).
parent(elizabeth, sophia).
parent(sophia, george1).
parent(charles1, catherine).
parent(charles1, charles2).
parent(charles1, james2).
parent(james1, elizabeth).

mother(X,Y):-female(X),parent(X,Y).
father(X,Y):-male(X),parent(X,Y).


sister(X,Y):-female(X),parent(Z,X),parent(Z,Y).
brother(X,Y):-male(X),parent(Z,X),parent(Z,Y).
aunt(X,Y):-female(X),parent(Z,Y),sister(X,Z).

