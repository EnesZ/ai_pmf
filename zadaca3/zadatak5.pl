len([], 0).
len([_|T], N) :- len(T, Y), N is Y + 1.

temp_f(X,Y):-X=:=2*Y.

f(L1,L2):-len(L1,R1),len(L2,R2),temp_f(R1,R2).
