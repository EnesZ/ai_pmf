max([T],T).
max([T|Rest],R):-max(Rest,S),(T>S->R=T;R=S).

min([T],T).
min([T|Rest],R):-min(Rest,S),(T<S->R=T;R=S).

range(T,R):-max(T,R1),min(T,R2),R is R1-R2.

