from timeit import default_timer as timer
import matplotlib.pylab as plt
import pylab
from zadaca4.minimax import minimax
from zadaca4.alfa_beta import alphabeta

n = 17

x = range(1,n+1)
minimax_time = []
alphabeta_time = []

for i in range(1,n+1):
    start = timer()
    minimax([i],True)
    end = timer()

    t = end - start
    minimax_time.append(t)

    start = timer()
    alphabeta([i],True,-2,2)
    end = timer()

    t = end-start
    alphabeta_time.append(t)

plt.plot(x, minimax_time,label= 'minimax')
plt.plot(x, alphabeta_time, label= 'alpha-beta')
pylab.legend(loc='upper left')
pylab.xlabel('Number of coins')
pylab.ylabel('Time in seconds')
plt.savefig('Comparison, n={}'.format(n))
plt.show()