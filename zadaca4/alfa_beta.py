
def divide_coins(pile_list):
    all_pile_lists=[]
    for i in range(len(pile_list)):
        if pile_list[i]<3:
            continue
        j=1
        while True:
            k = pile_list[i]-j
            if j>=k:
                break
            new_pile_list = pile_list.copy()
            new_pile_list[i]=j
            new_pile_list.append(k)
            all_pile_lists.append(new_pile_list)
            j+=1
    if len(all_pile_lists)>0:
        return all_pile_lists
    return None

def alphabeta(pile_list, maximizingPlayer, alpha, beta):

    if maximizingPlayer:
        maxEval = [-2]
        pile_lists = divide_coins(pile_list)
        if pile_lists==None:
            return -1
        for p_l in pile_lists:

            eval= alphabeta(p_l,False, alpha, beta)
            maxEval.append(eval)
            alpha = max(alpha,eval)
            if beta <= alpha:
                break
        return max(maxEval)
    else:
        minEval = [2]
        pile_lists = divide_coins(pile_list)
        if not pile_lists:
            return 1
        for p_l in pile_lists:
            eval = alphabeta(p_l, True, alpha, beta)
            minEval.append(eval)
            beta = min(beta, eval)
            if beta <= alpha:
                break
        return min(minEval)


n = 4
x = alphabeta([n],True,-2,2)

print("Za dato pocetnih {} kovanica, sa pretpostavkom da igramo prvi i da protivnik\
 igra optimalno,ovu igru {} pobijediti".format(n,"mozemo" if x>0 else "ne mozemo"))




