import networkx as nx
import matplotlib.pyplot as plt
from networkx.drawing.nx_agraph import write_dot, graphviz_layout


class Index:
    i = 0
def divide_coins(pile_list):
    all_pile_lists=[]
    for i in range(len(pile_list)):
        if pile_list[i]<3:
            continue
        j=1
        while True:
            k = pile_list[i]-j
            if j>=k:
                break
            new_pile_list = pile_list.copy()
            new_pile_list[i]=j
            new_pile_list.append(k)
            all_pile_lists.append(new_pile_list)
            j+=1
    if len(all_pile_lists)>0:
        return all_pile_lists
    return None
G = nx.DiGraph()
def minimax(pile_list, maximizingPlayer,index,G):
    index.i+=1
    num_index = index.i

    if maximizingPlayer:
        node_names =[]
        maxEval = [-2]
        pile_lists = divide_coins(pile_list)
        if pile_lists==None:
            return -1,str(pile_list),True
        for p_l in pile_lists:

            eval,n_index, leaf = minimax(p_l,False,index,G)
            if leaf:
                G.add_node("({}): {}".format(str(p_l),eval))
            node_names.append("({}): {}".format(n_index,eval))

            maxEval.append(eval)
        m = max(maxEval)
        G.add_node("({}): {}".format(str(pile_list), m))
        for name in node_names:
            G.add_edge("({}): {}".format(str(pile_list), m),name)
        return m,str(pile_list),False
    else:
        node_names = []
        minEval = [2]
        pile_lists = divide_coins(pile_list)
        if not pile_lists:
            return 1,str(pile_list),True
        for p_l in pile_lists:
            eval,n_index, leaf = minimax(p_l, True,index,G)
            if leaf:
                G.add_node("({}): {}".format(str(p_l),eval))
            node_names.append("({}): {}".format(n_index,eval))
            minEval.append(eval)
        m = min(minEval)
        G.add_node("({}): {}".format(str(pile_list), m))
        for name in node_names:
            G.add_edge("({}): {}".format(str(pile_list), m), name)
        return m, str(pile_list), False


n = 6
index = Index()
x,_,_ = minimax([n],True,index,G)

nx.draw_networkx(G, pos = nx.shell_layout(G),font_size=6)
nx.draw_networkx_edge_labels(G, pos = nx.shell_layout(G),font_size=6)
print("Za dato pocetnih {} kovanica, sa pretpostavkom da igramo prvi i da protivnik\
 igra optimalno,ovu igru {} pobijediti".format(n,"mozemo" if x>0 else "ne mozemo"))
plt.show()
"""
### draw tree structure
# https://stackoverflow.com/questions/11479624/is-there-a-way-to-guarantee-hierarchical-output-from-networkx
# write dot file to use with graphviz
# run "dot -Tpng test.dot >test.png"

### requires pygraphviz and visual studio 15+
write_dot(G,'test.dot')
# same layout using matplotlib with no labels
#plt.title('draw_networkx')
#pos =graphviz_layout(G, prog='dot')
#nx.draw(G, pos, with_labels=False, arrows=True)
#plt.savefig('nx_test.png')
"""



