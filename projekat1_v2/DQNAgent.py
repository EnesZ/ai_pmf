"""
Deep Q-Learning with Keras
References: https://keon.io/deep-q-learning/
"""

import numpy as np
from collections import deque
from keras.models import Sequential
from keras.layers import Dense,BatchNormalization, Dropout
from keras.optimizers import Adam
from keras.callbacks import History
from keras import backend as K
import random
import tensorflow as tf
from keras.initializers import VarianceScaling
history = History()


EPISODES = 1000

class DQNAgent:
    def __init__(self, state_size, action_size):
        self.state_size = state_size
        self.action_size = action_size
        self.memory = deque(maxlen=2000)
        self.positive_memory = deque(maxlen=100)
        self.negative_memory = deque(maxlen=100)
        self.gamma = 0.95    # discount rate
        self.epsilon = 1.0  # exploration rate
        self.epsilon_min = 0.01
        self.epsilon_decay = 0.99
        self.learning_rate = 0.005
        self.model = self._build_model()
        self.model_history = deque(maxlen=500)

    def _huber_loss(self, y_true, y_pred, clip_delta=1.0):
        error = y_true - y_pred
        cond = K.abs(error) <= clip_delta

        squared_loss = 0.5 * K.square(error)
        quadratic_loss = 0.5 * K.square(clip_delta) + clip_delta * (K.abs(error) - clip_delta)

        return K.mean(tf.where(cond, squared_loss, quadratic_loss))

    def _build_model(self):
        # Neural Net for Deep-Q learning Model
        model = Sequential()
        #model.add(BatchNormalization())
        model.add(Dense(64, input_dim=self.state_size,activation='relu',use_bias=False, kernel_initializer=VarianceScaling(scale=2.0)))
        #model.add(Dropout(0.3))
        #model.add(BatchNormalization())
        model.add(Dense(32, activation='relu',use_bias=False ,kernel_initializer=VarianceScaling(scale=2.0)))
        #model.add(Dropout(0.3))
        #model.add(BatchNormalization())

        model.add(Dense(self.action_size, activation='linear'))
        model.compile(loss=self._huber_loss,
                      optimizer=Adam(lr=self.learning_rate))
        return model

    def remember(self, state, action, reward, next_state, done):

        if reward==-1:
            self.negative_memory.append((state, action, reward, next_state, done))
        elif reward >= 5:
            self.positive_memory.append((state, action, reward, next_state, done))
        else:
            self.memory.append((state, action, reward, next_state, done))

    def act(self, state):
        if np.random.rand() <= self.epsilon:
            return random.randrange(self.action_size)
        act_values = self.model.predict(state)
        #print(act_values[0])
        return np.argmax(act_values[0])  # returns action

    def replay(self, batch_size):
        print(self.epsilon)
        k = 200
        if k>len(self.positive_memory):
            k=len(self.positive_memory)
        minibatch1 = random.sample(self.memory, k)
        k=6
        if k>len(self.positive_memory):
            k=len(self.positive_memory)
        minibatch2 = random.sample(self.positive_memory,k)
        k = 6
        if k>len(self.negative_memory):
            k=len(self.negative_memory)
        minibatch3 = random.sample(self.negative_memory,k)
        minibatch = minibatch1+minibatch2
        minibatch = minibatch+minibatch3
        random.shuffle(minibatch)

        for state, action, reward, next_state, done in minibatch:
            target = reward
            if not done:
                target = (reward + self.gamma *
                          np.amax(self.model.predict(next_state)[0]))
            target_f = self.model.predict(state)
            target_f[0][action] = target
            history=self.model.fit(state, target_f, epochs=1, verbose=0)
            self.model_history.append(history.history['loss'])
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay

    def load(self, name):
        self.model.load_weights(name)

    def save(self, name):
        self.model.save_weights(name)