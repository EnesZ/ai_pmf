import pygame as pg
from numpy import sqrt,abs,reshape
from projekat1_v2.DQNAgent import DQNAgent,EPISODES

WIDTH = 800
HEIGHT = 400
EPSILON = 0.5
CAR_RADIUS = 15

pg.font.init()
FONT = pg.font.SysFont("Consolas",20)

#colors
WHITE = (255,255,255)
YELLOW = (255, 200, 0)
BLACK = (0,0,0)
ORANGE = (255,100,0)
RED = (255,0,0)
GREEN = (150,150,0)
GRAY = (211,211,211)
BLUE = (0,0,100)


pg.init()

screen = pg.display.set_mode((WIDTH, HEIGHT))
clock = pg.time.Clock()

state_size = 9
action_size = 3
agent = DQNAgent(state_size,action_size)

#myimage = pg.image.load("res/car.png")
#myimage = pg.transform.scale(myimage, (20,20))


def ori(p1,p2,p3):
    # x1(y2-y3)+x2(y3-y1)+x3(y1-y2)
    temp = p1[0]*(p2[1]-p3[1]) + p2[0]*(p3[1]-p1[1]) + p3[0]*(p1[1]-p2[1])
    if temp>0:
        return 1
    elif temp<0:
        return -1
    else:
        return 0

def segment_intersection(s1,s2):
    if ori(s1[0],s1[1],s2[0])==ori(s1[0],s1[1],s2[1]):
        return False
    if ori(s2[0],s2[1],s1[0])==ori(s2[0],s2[1],s1[1]):
        return False
    return True

def poly_intersection(poly1,poly2):
    for i in range(len(poly1)):
        e1 = (poly1[i-1],poly1[i])
        for j in range(len(poly2)):
            if segment_intersection(e1,(poly2[j-1],poly2[j])):
                return True
    return False
def rot_center(image, angle):
    """rotate an image while keeping its center and size"""
    orig_rect = image.get_rect()
    rot_image = pg.transform.rotate(image, angle)
    rot_rect = orig_rect.copy()
    rot_rect.center = rot_image.get_rect().center
    rot_image = rot_image.subsurface(rot_rect).copy()
    return rot_image

def p2p_line(p1,p2):
    #line through 2 points
    if p2[0]-p1[0]==0:
        a = (p2[1]-p1[1])/0.0001
    else:
        a = (p2[1]-p1[1])/(p2[0]-p1[0])
    if (p2[0]-p1[0])==0:
        b = (p1[1]*p2[0]-p2[1]*p1[0])/0.0001
    else:
        b = (p1[1]*p2[0]-p2[1]*p1[0])/(p2[0]-p1[0])
    return a,b

def line_intersection(a1,b1,a2,b2):
    #if given two lines, find points of intersection
    x = (b2-b1)/(a1-a2)
    y = (a1*b2-a2*b1)/(a1-a2)
    return x,y

def distance(p1,p2):
    # euclidean distance between two points
    return sqrt((p1[0]-p2[0])*(p1[0]-p2[0])+(p1[1]-p2[1])*(p1[1]-p2[1]))

def segment_center(p1,p2):
    # returns center point of segment
    x = abs(p1[0]+p2[0])/2
    y = abs(p1[1]+p2[1])/2
    return x,y


def p2s_distance(p,s):
    # point to segment distance
    a,b = p2p_line(s[0],s[1])

    # normal line to y=ax+b
    if a==0:
        a1 = -1/(a+0.0001)
    else:
        a1 = -1 / (a + 0.0001)
    if a==0:
        b1 = p[0]/(a+0.0001)+p[1]
    else:
        b1 = p[0] /a + p[1]

    # intersection between y and y_norm
    x,y = line_intersection(a,b,a1,b1)

    # if x,y is between s[0] and s[1] distance is d(p,(x,y))
    # else distance is min(d(s[0],p),d(s[1],p))
    if is_between((x,y),s[0],s[1]):
        return distance(p,(x,y))

    return min(distance(s[0],p),distance(s[1],p))
def is_between(p,p1,p2):
    # returns is point p between p1 and p2
    # with the assumption that p,p1,p2 are collinear
    # we only need to check for x-coord or y-coord
    if p[0]>p1[0] and p[0]>p2[0]:
        return False
    if p[0]<p1[0] and p[0]<p2[0]:
        return False
    return True

def analyze_action(action):
    i=0
    j=0
    k=0
    n = len(action)
    for a in action:
        if a==0:
            i+=1
        elif a==1:
            j+=1
        else:
            k+=1

    print("left - {}, right - {}, straight - {}".format(i/n,j/n,k/n))



