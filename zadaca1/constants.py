import pygame as pg
from numpy import sqrt
from time import sleep

WIDTH = 800
HEIGHT = 400
COIN_R = 10
TABLE_R = 100
TABLE_C = (int(WIDTH/2),int(HEIGHT/2))

pg.font.init()
FONT = pg.font.SysFont("Consolas",20)

#colors
WHITE = (255,255,255)
YELLOW = (255, 200, 0)
BLACK = (0,0,0)
ORANGE = (255,100,0)
RED = (255,0,0)

class Coin():
    def __init__(self,x,y,r):
        self.x = x
        self.y = y
        self.r = r
    def get_center(self):
        return (self.x,self.y)


def intersection(c1,c2):
    d = sqrt((c1.x-c2.x)**2+(c1.y-c2.y)**2)
    if d < 2*COIN_R:
        return True
    else:
        return False

def check_intersection(coin_list,c1):
    for c in coin_list:
        if intersection(c,c1):
            return True
    return False