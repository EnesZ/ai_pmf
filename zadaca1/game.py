from constants import *

assert COIN_R < TABLE_R
pg.init()

screen = pg.display.set_mode((WIDTH, HEIGHT))
clock = pg.time.Clock()
running =True

coin_list = []
first_move = True
player_move = False
(mouse_x,mouse_y) = TABLE_C
msg1 = 'Wait...'
msg2 = ''
start_time = pg.time.get_ticks()
player_time = 20
while running:
    screen.fill(BLACK)
    pg.draw.circle(screen, WHITE, TABLE_C, TABLE_R)
    for event in pg.event.get():
        if event.type == pg.KEYDOWN:
            if event.key == pg.K_ESCAPE:
                running = False
        if event.type == pg.MOUSEMOTION:
            mouse_x,mouse_y = event.pos
        if event.type == pg.MOUSEBUTTONDOWN and player_move:
            x,y = event.pos
            if sqrt((x-TABLE_C[0])**2+(y-TABLE_C[1])**2)> TABLE_R-COIN_R:
                #print("You should put the coin on the table!")
                msg2 = "You should put the coin on the table!"
            else:
                c = Coin(x,y,COIN_R)
                if check_intersection(coin_list,c):
                    #print("Wrong move. No coin can touch a coin which is already on the table!")
                    msg2 = "Wrong move. No coin can touch a coin which is already on the table!"
                else:
                    coin_list.append(c)
                    player_move = False
                    msg1 = 'Wait...'
                    msg2 = ''
    for c in coin_list:
        pg.draw.circle(screen,YELLOW,c.get_center(),COIN_R)

    if not player_move:
        if first_move:
            # put coin on the center of the table
            new_coin = Coin(*TABLE_C, COIN_R)
            first_move = False
        else:
            # take the last coin from the list and
            # create central symmetric point with respect to the table center
            last_coin = coin_list[-1]
            c_x = 2 * TABLE_C[0] - last_coin.x
            c_y = 2 * TABLE_C[1] - last_coin.y
            new_coin = Coin(c_x, c_y, COIN_R)

        coin_list.append(new_coin)
        player_move = True
        msg1 = 'Your move...'
        start_time = pg.time.get_ticks()

    pg.draw.circle(screen, ORANGE, (mouse_x, mouse_y), COIN_R)
    screen.blit(FONT.render(msg1, True, WHITE), (2, 20))
    screen.blit(FONT.render(msg2,True,RED),(2,40))
    if start_time:
        time_since_enter = (pg.time.get_ticks() - start_time) / 1000
        msg1 = 'Time left {}'.format(int(player_time-time_since_enter))
        if time_since_enter > player_time:
            screen.blit(FONT.render('', True, RED), (2, 40))
            screen.blit(FONT.render("GAME OVER", True, RED), (2,60))
            running=False
    pg.display.flip()

sleep(3)
pg.quit()